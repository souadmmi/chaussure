package co.ecommerce.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Picture;

@Repository
public interface PictureRepository extends JpaRepository<Picture, Integer>{

}
