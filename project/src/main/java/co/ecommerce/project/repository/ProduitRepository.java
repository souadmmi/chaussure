package co.ecommerce.project.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Integer> {

    @Query("SELECT p FROM Produit p WHERE  p.name LIKE %?1% OR p.marque LIKE %?1%")
    List<Produit> findSearch(String search);

}