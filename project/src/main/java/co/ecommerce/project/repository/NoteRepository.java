package co.ecommerce.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.ecommerce.project.entity.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer>  {

}
