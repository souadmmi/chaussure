package co.ecommerce.project.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Commande;
import co.ecommerce.project.repository.CommandeRepository;

@RestController
@RequestMapping("/api/commande")
public class CommandeController {
    @Autowired
    private CommandeRepository repo;

    @GetMapping
    public List<Commande> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Commande one(@PathVariable int id) {
        return repo.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Commande add(@Validated @RequestBody Commande commande) {
        return repo.save(commande);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Commande commande = one(id);
        repo.delete(commande);
    }

    @PutMapping("/{id}")
    public Commande update(@PathVariable int id, @Validated @RequestBody Commande commande) {
        Commande toUpdate = one(id);

        toUpdate.setDate(commande.getDate());
        toUpdate.setPrix(commande.getPrix());
        toUpdate.setPanier(commande.getPanier());
        toUpdate.setUser(commande.getUser());

        return repo.save(toUpdate);
    }
}
