package co.ecommerce.project.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Taille;
import co.ecommerce.project.repository.TailleRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/taille")
public class TailleController {
    @Autowired
    private TailleRepository repo;

    @GetMapping
    public List<Taille> all() {
        return repo.findAll();
    }
    
    @GetMapping("/{id}")
    public Taille one(@PathVariable int id) {
        return repo.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Taille add(@Valid @RequestBody Taille taille) {
        repo.save(taille);
        return taille;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Taille taille = one(id);
        repo.delete(taille);
    }

    @PutMapping("/{id}")
    public Taille update(@PathVariable int id, @Valid @RequestBody Taille taille) {
        Taille toUpdate = one(id);

        toUpdate.setTaille(taille.getTaille());
        toUpdate.setStock(taille.getStock());
        toUpdate.setPanier(taille.getPanier());
        toUpdate.setProduit(taille.getProduit());
        
        return repo.save(toUpdate);
    }

}
