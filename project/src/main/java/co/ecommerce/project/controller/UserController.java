package co.ecommerce.project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.User;
import co.ecommerce.project.repository.UserRepository;
import jakarta.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/api/account")
    public User account(@AuthenticationPrincipal User user) {
        return user;
    }

    @PostMapping("/api/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User register(@Valid @RequestBody User user) {
        if(userRepo.findByEmail(user.getEmail()).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
        }
        user.setRole("ROLE_USER");
        user.setPassword(encoder.encode(user.getPassword()));
        userRepo.save(user);
        return user;
    }
    @GetMapping("/api/user")
    public List<User> allUsers(){
        return userRepo.findAll();
    }

    @GetMapping("api/user/{id}")
    public User oneUser(@PathVariable int id){
        Optional <User> user = userRepo.findById(id);
        if (user == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return user.get();
    }


    @DeleteMapping("api/user/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id, @AuthenticationPrincipal User user) {
        if(!user.getRole().equals("ROLE_ADMIN") && user.getRole().equals("ROLE_SUPER_ADMIN")) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    userRepo.delete(oneUser(id));
    }
    


}

