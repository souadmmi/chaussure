package co.ecommerce.project.controller;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;


import co.ecommerce.project.entity.Picture;
import co.ecommerce.project.repository.PictureRepository;

@RestController
@RequestMapping("/api/picture")
public class PictureController {

    @Autowired
    private PictureRepository repo;

    @GetMapping
    public List<Picture> all() {
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Picture one(@PathVariable int id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Picture add(@Validated @RequestBody Picture picture) {
        return repo.save(picture);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Picture picture = one(id);
        repo.delete(picture);
    }

    @PutMapping("/{id}")
    public Picture update(@PathVariable int id, @Validated @RequestBody Picture picture) {
        Picture toUpdate = one(id);

        toUpdate.setImage(picture.getImage());
        toUpdate.setProduit(picture.getProduit());
        return repo.save(toUpdate);
    }

    @PostMapping("/upload")
    public String upload(@RequestParam MultipartFile image) {
        String renamed = UUID.randomUUID() + ".jpg";
        try {
            Thumbnails.of(image.getInputStream())
                    .width(900)
                    .toFile(new File(getUploadFolder(), renamed));
            Thumbnails.of(image.getInputStream())
                    .size(200, 200)
                    .crop(Positions.CENTER)
                    .toFile(new File(getUploadFolder(), "thumbnail-" + renamed));
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Upload error", e);
        }
        return renamed;
    }

    private File getUploadFolder() {
        File folder = new File(getClass().getClassLoader().getResource(".").getPath().concat("static/uploads"));
        if (!folder.exists()) {
            folder.mkdirs();
        }
        return folder;

    }

}
