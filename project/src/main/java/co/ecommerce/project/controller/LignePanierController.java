package co.ecommerce.project.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.LignePanier;
import co.ecommerce.project.repository.LignePanierRepository;


@RestController
@RequestMapping("/api/lignePanier")
public class LignePanierController {

    @Autowired
    private LignePanierRepository repo;

    @GetMapping
    public List<LignePanier> all() {
        return repo.findAll(); 
    }

    @GetMapping("/{id}")
    public LignePanier one(@PathVariable int id) {
        return repo.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "LignePanier not found"));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public LignePanier add(@Validated @RequestBody LignePanier lignePanier) {
        return repo.save(lignePanier);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        LignePanier lignePanier = one(id);
        repo.delete(lignePanier);
    }

    @PutMapping("/{id}")
    public LignePanier update(@PathVariable int id, @Validated @RequestBody LignePanier lignePanier) {
        LignePanier toUpdate = one(id);
        toUpdate.setQuantity(lignePanier.getQuantity());
        toUpdate.setPrix(lignePanier.getPrix());
        toUpdate.setTaille(lignePanier.getTaille());
        toUpdate.setCommande(lignePanier.getCommande());
        return repo.save(toUpdate);
    }
}
