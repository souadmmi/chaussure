package co.ecommerce.project.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.ecommerce.project.entity.Produit;
import co.ecommerce.project.entity.User;
import co.ecommerce.project.repository.ProduitRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/produit")
public class ProduitController {

    @Autowired
    private ProduitRepository repo;

    @GetMapping
    public List<Produit> all(@RequestParam(required = false) String search) {
        if(search != null) {
            return repo.findSearch(search);
        }
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Produit one(@PathVariable int id) {
        return repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produit add(@Valid @RequestBody Produit produit, @AuthenticationPrincipal User user) {
        if(user.getRole() != "ROLE_ADMIN" ||user.getRole() != "ROLE_SUPER_ADMIN"){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        repo.save(produit);
        return produit;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id, @AuthenticationPrincipal User user) {
        Produit prod = one(id);
        if(user.getRole() != "ROLE_ADMIN" ||user.getRole() != "ROLE_SUPER_ADMIN"){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        repo.delete(prod);

    }

    @PutMapping("/{id}")
    public Produit update (@PathVariable int id, @Valid @RequestBody Produit produit, @AuthenticationPrincipal User user) {
        Produit toUpdate = one(id);
        if(user.getRole() != "ROLE_ADMIN" ||user.getRole() != "ROLE_SUPER_ADMIN"){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        toUpdate.setName(produit.getName());
        toUpdate.setDescription(produit.getDescription());
        toUpdate.setImage(produit.getImage());
        toUpdate.setPrix(produit.getPrix());
        toUpdate.setMarque(produit.getMarque());
        toUpdate.setCategorie(produit.getCategorie());
        return toUpdate; 
    
}

}