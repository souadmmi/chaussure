package co.ecommerce.project.entity;


import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class Commande {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
private LocalDateTime date;
private Integer prix;
@ManyToOne 
private LignePanier panier;
@ManyToOne
private User user;

public Commande(LocalDateTime date, Integer prix, LignePanier panier, User user) {
    this.date = date;
    this.prix = prix;
    this.panier = panier;
    this.user = user;
}
public Commande(Integer id, LocalDateTime date, Integer prix, LignePanier panier, User user) {
    this.id = id;
    this.date = date;
    this.prix = prix;
    this.panier = panier;
    this.user = user;
}
public Commande() {
}
public Integer getId() {
    return id;
}
public void setId(Integer id) {
    this.id = id;
}
public LocalDateTime getDate() {
    return date;
}
public void setDate(LocalDateTime date) {
    this.date = date;
}
public Integer getPrix() {
    return prix;
}
public void setPrix(Integer prix) {
    this.prix = prix;
}
public LignePanier getPanier() {
    return panier;
}
public void setPanier(LignePanier panier) {
    this.panier = panier;
}
public User getUser() {
    return user;
}
public void setUser(User user) {
    this.user = user;
}
}
