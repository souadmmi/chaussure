package co.ecommerce.project.entity;


import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotBlank;

@Entity
public class LignePanier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    private Integer quantity;
    private Integer prix;
    @OneToMany (mappedBy = "panier")
    private List<Taille> taille = new ArrayList<>();
    @OneToMany (mappedBy = "panier")
    private List<Commande> commande = new ArrayList<>();
    
    public LignePanier(@NotBlank Integer quantity, Integer prix, List<Taille> taille, List<Commande> commande) {
        this.quantity = quantity;
        this.prix = prix;
        this.taille = taille;
        this.commande = commande;
    }
    public LignePanier() {
    }
    public LignePanier(Integer id, @NotBlank Integer quantity, Integer prix, List<Taille> taille,
            List<Commande> commande) {
        this.id = id;
        this.quantity = quantity;
        this.prix = prix;
        this.taille = taille;
        this.commande = commande;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    public Integer getPrix() {
        return prix;
    }
    public void setPrix(Integer prix) {
        this.prix = prix;
    }
    public List<Taille> getTaille() {
        return taille;
    }
    public void setTaille(List<Taille> taille) {
        this.taille = taille;
    }
    public List<Commande> getCommande() {
        return commande;
    }
    public void setCommande(List<Commande> commande) {
        this.commande = commande;
    }
}
